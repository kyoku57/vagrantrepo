# ToolBox

This project contains a simple docker-compose to quickly run some services.


## How to run the toolbox

### Main files

The project contains 2 files:
- docker-compose.yml: use to run the differents services
- Vagrantfile: use to initiate a Virtuabox with docker and docker-compose in it

### Docker friendly environment

Just run the following script:
```bash
$ docker-compose up -d
```

### No docker friendly environment

Place the two file at the common directory of your projects.  

Run the following script:
```bash
$ cd /your/common/directory
total 5
drwxr-xr-x 1 sebastien 1049089     0 oct.  10 10:01 myfirstproject/
drwxr-xr-x 1 sebastien 1049089     0 oct.  10 09:56 mysecondproject/
drwxr-xr-x 1 sebastien 1049089     0 sept.  3 14:27 mythirdproject/
-rw-r--r-- 1 sebastien 1049089  3080 oct.  31 11:37 docker-compose.yml
-rw-r--r-- 1 sebastien 1049089  2265 oct.  31 11:43 Vagrantfile

$ vagrant up
```

Enter in your VM and in your shared directory
```bash
$ vagrant ssh
[vagrant@localhost]$ cd /vagrant
[vagrant@localhost]$ ls -l
total 5
drwxr-xr-x 1 sebastien 1049089     0 oct.  10 10:01 myfirstproject/
drwxr-xr-x 1 sebastien 1049089     0 oct.  10 09:56 mysecondproject/
drwxr-xr-x 1 sebastien 1049089     0 sept.  3 14:27 mythirdproject/
-rw-r--r-- 1 sebastien 1049089  3080 oct.  31 11:37 docker-compose.yml
-rw-r--r-- 1 sebastien 1049089  2265 oct.  31 11:43 Vagrantfile
```

You just need to finish with this command:
```bash
[vagrant@localhost]$ docker-compose up -d
```

If you don't use the VM anymore, you can quit it (or destroy if you want to remove the VM)
```bash
[vagrant@localhost]$ exit
$ vagrant halt
$ vagrant destroy
```

> **Note**: be careful on the place of the Vagrantfile. Once you have one instance of the VM, you can't change the directory of the Vagrantfile.


## Manage the services

Once the docker-compose command is launched, with the following command you can have a status of docker containers:

```bash
[vagrant@localhost]$ docker ps -a
CONTAINER ID        IMAGE                                      COMMAND                  CREATED             STATUS              PORTS                                            NAMES
60d164cadd3f        phpmyadmin/phpmyadmin                      "/docker-entrypoint.…"   46 minutes ago      Up 46 minutes       0.0.0.0:9000->80/tcp                             myphpmyadmin
be3316621bcd        dpage/pgadmin4                             "/entrypoint.sh"         46 minutes ago      Up 46 minutes       443/tcp, 0.0.0.0:9001->80/tcp                    mypgadmin
2fa182db548b        postgres                                   "docker-entrypoint.s…"   46 minutes ago      Up 46 minutes       0.0.0.0:5432->5432/tcp                           mypostgres
...
```

Some usefull commands
```bash
# list containers
$ docker ps -a

# view consumption of containers
$ docker stats

# view log of containers
$ docker logs <your_container>

# remove containers
$ docker-compose down

# remove containers with volumes
$ docker-compose down --volumes

# list volumes available
$ docker volume ls

# connect to a volume to see its content
$ docker run -it -v <your_volume_name>:/tmp ubuntu bash

# copy file from your docker to your machine
$ docker cp <your_container>:/<your_path> .
```


## Available services

### Container


| Tools         | Container             | version   | service/port          | descriptions/notes 
|-              |-                      |-          |-                      |-
| MariaDB       | mymariadb             |latest     | 3307                  | mydatabase<br>myuser / password
| MySQL         | mymysql               | 5.7       | 3306                  | mydatabase<br>myuser / password
| PostGreSQL    | mypostgre             | latest    | 5432                  | mydatabase<br>myuser / password
| SQL Server    | mymssql               | latest    | 1433                  | master/dbo<br>myuser / MyPassword!
|               |                       |           |                       |
| phpmyadmin    | myphpmyadmin          | latest    | http://localhost:9000 | Web interface, work only for mysql<br>root / mypassword
| pgadmin4      | mypgadmin             | latest    | http://localhost:9001 | Web interface work only for postgres<br>sebastien.dupire@intech.lu / mypassword
| phpmyadmin    | myphpmyadminformariadb| latest    | http://localhost:9002 | Web interface, work only for mariadb<br>root / mypassword
|               |                       |           |                       | 
| MailHog       | mymailhog             | latest    | SMTP: 1025<br>http://localhost:9100   | SMTP Server + Web Interface (1)
| SFTP          | mysftp                | latest    | 7022                  | SFTP Server with one user<br>sftp / mypassword
| Squid         | myproxyfree           | latest    | http://localhost:7100 | Proxy server to test VM proxy
| Squid         | myproxybasic          | latest    | http://localhost:7101 | Proxy server to test VM proxy with basic authentication<br> myuser / mypassword



### Volumes

Volume are used to persist data in the differents containers.

| Volume        | Container     | Description
|-              |-              |-
| mysql_data    | mymysql       | persist the db
| mariadb_data  | my            | persist the db
| postgres_data |               | persist the db
| mssql_data    |               | persist the db
| pgadmin_data  |               | persist the configuration of pgadmin
| sftp_data     | mysftp        | persist the *incoming* directory 


### Networks

A network named **mynetwork** is created to link all the containers.


## Annexes


### (1)- MailHog Configuration Spring for MailHog

```properties
spring.mail.host=localhost
spring.mail.port=1025
spring.mail.username=no.reply@yourdomain.lu
spring.mail.password=

spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.connectiontimeout=5000
spring.mail.properties.mail.smtp.timeout=5000
spring.mail.properties.mail.smtp.writetimeout=5000
spring.mail.properties.mail.smtp.starttls.enable=true
```

## Recommanded external tools

| Tools         | link                  | description 
|-              |-                      |- 
| DBeaver       |https://dbeaver.io/    | db client application to connect to all type of database.

## References

- https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-configure-docker?view=sql-server-ver15
- https://mariadb.com/resources/blog/mariadb-and-docker-use-cases-part-1/
- https://hub.docker.com/r/writl/sftp/
- https://hub.docker.com/r/sameersbn/squid/