# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "bento/centos-7.3"
  config.vm.define "IBMVirtualMachine"

  config.vm.network "forwarded_port", guest: 50010, host: 50010 # DB2-10
  config.vm.network "forwarded_port", guest: 50011, host: 50011 # DB2-11
  config.vm.network "forwarded_port", guest: 9043, host: 9043 # WebSphere 8.5 - administration port : https://localhost:9043/ibm/localhost
  config.vm.network "forwarded_port", guest: 9443, host: 9443 # WebSphere 8.5 - applications port

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "8192"
    vb.cpus = 2
	  vb.customize ["modifyvm", :id, "--graphicscontroller", "vboxvga"]
	  vb.customize ["modifyvm", :id, "--accelerate3d", "on"]
	  vb.customize ["modifyvm", :id, "--ioapic", "on"]
	  vb.customize ["modifyvm", :id, "--vram", "128"]
	  vb.customize ["modifyvm", :id, "--hwvirtex", "on"]
    vb.name = "IBMVirtualMachine"
  end

  config.vm.provision "shell", inline: <<-SHELL
    # docker 
    sudo yum install -y yum-utils device-mapper-persistent-data lvm2
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install -y docker-ce
    sudo systemctl enable docker
    sudo systemctl start docker
    sudo groupadd docker
    sudo usermod -aG docker vagrant
    # docker compose
    sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    # tools
    sudo yum install -y maven unzip
    # default network
    docker network create --driver bridge mynetwork
  SHELL

  # ====================================================================
  # DB2 - 10 
  # ====================================================================
  # References :
  #  - https://vladmihalcea.com/how-to-install-db2-express-c-on-docker-and-set-up-the-jdbc-connection-properties/
  #  - https://hub.docker.com/r/ibmcom/db2express-c/
  # ====================================================================
  config.vm.provision "db2-10", type: "shell", run: "never", inline: <<-SHELL
    # Install DB2 10
    cd /vagrant/docker/db2-10
    docker build -f Dockerfile -t intech-db2-10-image .
    docker rm -f db2-10-instance
    docker run -d --name db2-10-instance -h db2-10-instance --restart always -p 50010:50000 \
        --network mynetwork \
        -e DB2INST1_PASSWORD=db2container \
        -e LICENSE=accept \
        -e DATABASENAME=MYDB \
        -e DEFAULTSCHEMA=MYSCHEMA \
        intech-db2-10-image:latest db2start 
    docker exec db2-10-instance /opt/scripts/create-db.sh
  SHELL

  # ====================================================================
  # WebSphere 8.5 
  # ====================================================================
  # References :
  # - https://hub.docker.com/r/ibmcom/websphere-traditional
  # - https://github.com/WASdev/ci.docker.websphere-traditional#docker-hub-image 
  # ====================================================================
  config.vm.provision "websphere-8.5", type: "shell", run: "never", inline: <<-SHELL
    # Install Websphere
    cd /vagrant/docker/websphere-8.5
    docker build -f Dockerfile -t intech-websphere-8.5-image .
    docker rm -f websphere-8.5-instance
    docker run -d --name websphere-8.5-instance -h websphere-8.5-instance --restart always -p 9043:9043 -p 9443:9443 \
        --network mynetwork \
        -v /vagrant/docker/websphere-8.5/PASSWORD:/tmp/PASSWORD \
        intech-websphere-8.5-image:latest
  SHELL
end

