#!/bin/bash

# Database creation
su - db2inst1 -c 'db2 create db '${DATABASENAME}

# Replace default schema
sed -i -e 's/XXXXX/'${DEFAULTSCHEMA}'/g' /opt/sql/*.sql

# Play each scripts
for f in /opt/sql/*; do
  case "$f" in
    *.sh)     echo "$0: running $f"; . "$f" ;;
    *.sql)    echo "$0: running $f"; echo "exit" | su - db2inst1 -c 'db2 connect to '${DATABASENAME}'; db2 -tvmf "'$f'"; db2 terminate;'; echo ;;
    *)        echo "$0: ignoring $f" ;;
  esac
  echo
done