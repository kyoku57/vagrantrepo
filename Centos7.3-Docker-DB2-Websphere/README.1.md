# Installation ERCS virtual machine

## Base installation

Run the vagrant machine

```bash
vagrant up
```

## DB2-10

You can provision it with db2-10 instance

```bash
vagrant provision --provision-with db2-10
```

You can connect to the DB with DBViz  
https://www.dbvis.com/download/10.0

```
Database Type       = DB2 LUW
Driver (JDBC)       = DB2
Database Server     = localhost
Database Port       = 50010
Database            = LOCALDB
Database UserId     = db2inst1
Database Password   = db2container
```

To change password, you can edit *./Vagrantfile* in db2-10 provision configuration.

To change name of the Database, you can edit *./db2-10/scripts/sql/00-db_init.sql*

Other SQL scripts can be edited to fill your request.

## WebSphere 8.5

You can provision it with websphere 8.5 instance

```
vagrant provision --provision-with websphere-8.5
```

The admin console is available at https://localhost:9043/ibm/console

The user name is *wsadmin* and the default password is *wsadmin*.  
This password can be changed in the file *./docker/websphere-8.5/PASSWORD*

The deployed applications are available in https://localhost:9443/\<context-root>