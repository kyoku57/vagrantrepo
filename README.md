# Vagrant Repository

## Description

Just a repository with some VagrantFile ready to use. Each one is registered with this convention:

> /SystemVersion-MainToolsinIt

Every VM uses VirtualBox and shares the current host directory with VM directory */vagrant*


## Available vagrant machines

- *Ubuntu17.10-Angular* that contains
    - ???

- *CentOS7.3-Docker* that contains
    - Docker and docker-compose (version 3.3)

- *CentOS7.3-Docker-SMTP* that contains
    - Docker and docker-compose (version 3.7)
    - MariaDB
    - MySQL
    - PostGreSQL
    - SQL Server
    - Phpmyadmin
    - Mailhog
    - Squid

- *CentOS7.3-Docker-DB2-Websphere* that contains
    - Docker and docker-compose (version 3.7)
    - DB2 10
    - Websphere 8.5
